import React, { useState, useEffect } from "react";
import _ from "lodash";
import logo from "../../assets/Images/Logo.png";
import MODAL_LOGIN from "../ModalAuth/index";

function Navbar() {
  const [scrollY, setScrollY] = useState(0);
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  });

  const handleScroll = () => {
    setScrollY(window.setScrollY);
  };

  return (
    <nav className={`layout ${scrollY > 20 ? "layout--white" : ""}`}>
      <div className="layout__logo">
        <a href="/">
          <img src={logo} alt="logo" className="layout__img" />
        </a>

        <input type="checkbox" className="menu-btn" id="menu-btn" />
        <label htmlFor="menu-btn" className="menu-icon">
          <span className="menu-icon__line"></span>
        </label>
        <ul className="layout__list">
          <li>
            <a href="feature">Feature</a>
          </li>
          <li>
            <a href="about">About</a>
          </li>
        </ul>
      </div>
      <button className="layout__button">Ready To Sweat?</button>
    </nav>
  );
}

export default Navbar;
